"""Script to create the sensitivity analysis results graph for my master's thesis.

Some format declarations:
	Graphs have an outer border
	The first and last value is indented from the edge
	They are black and white
	Lines are fine"""

#  importing packages
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages

#  make initial definitions
font = 'Verdana'
size = 16
font_def = 'x-large'  # x-small, xx-small, medium, x-large, xx-large and so on.

#  make figure
figure = plt.figure(figsize=(12, 8))  # adjusts relative size

#  change markers
# plt.markers('o')

#  change legend
plt.xticks([-50, -25, 0, 25, 50], fontname=font, fontsize=font_def)
plt.yticks(fontname=font, fontsize=font_def)

#  english version
'''
plt.xlabel('change of the parameter [%]', fontname=font, fontsize=size)
plt.ylabel('difference in hydraulic head to base model 2030 [m]', fontname=font, fontsize=size)
'''
#  german version
plt.xlabel('Änderung des Parameters [%]', fontname=font, fontsize=size)
plt.ylabel('Grundwasserstand im Vergleich zum Basismodell in 2030 [m]', fontname=font, fontsize=size)


#  creates a graph from a given csv file
def create_plot(file, color, name):
    df = pd.read_csv(file, delimiter=";")
    x = df['variation']
    values = df['change']
    plt.plot(x, values, 'k-', label=file[:-4], marker='o', color=color)


###  MAIN PROGRAM ###
if __name__ == "__main__":
    #  create all five graphs in the same figure
    create_plot('intransfer.csv', 'DarkBlue', 'intransfer')
    create_plot('rain.csv', 'Red', 'rain')
    create_plot('kx.csv', 'Black', 'kx')
    create_plot('abst.csv', 'Orange', 'abst')
    create_plot('wl.csv', 'Green', 'wl')

    #  add a legend
    plt.legend(frameon=False, fontsize=font_def, loc='upper left')

    #  add a grid
    plt.grid()

    #  finish and save to .pdf file
    filename = 'plot_all.pdf'
    pp = PdfPages(filename)
    print('\n\tGraph saved as %s.' % filename)
    pp.savefig(figure, dpi=300,
               bbox_inches='tight')  # saves as vector pdf, bbox eliminates white space around the graph
    pp.close()
    plt.close(figure)
    print('\n\n\tAll done.\n\n')